#!/usr/bin/env python3
# -*- coding: utf-8 -*-



def add_two_numbers(x:float=None, y:float=None) -> float:
    """Create a pandas `DataFrame` from a dictionary

    If either `x` or `y` is `None`, the function returns `None`. 
    Parameters
    ----------
    x : float, optional
        One value, by default None

    Y : float, optional
        Another value, by default None

    Returns
    -------
    float
        The sum of the two numbers
    """
    ret_val = None
    if x is not None and y is not None:
        ret_val = x + y

    return ret_val

