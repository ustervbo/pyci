#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import project.do_something as do_something


def test_add_two_numbers():
    assert do_something.add_two_numbers() is None
    assert do_something.add_two_numbers(x=1) is None
    assert do_something.add_two_numbers(y=5) is None

    res = do_something.add_two_numbers(x=1, y=5)
    assert res == 6
    assert isinstance(res, int)

    res = do_something.add_two_numbers(x=1.1, y=5.5)
    assert res == 6.6
    assert isinstance(res, float)


def test_failure():
    assert True
