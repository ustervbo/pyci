# pyci

[![Pipeline](http://gitlab.com/ustervbo/pyci/-/jobs/artifacts/main/raw/pipeline.svg?job=create_badges)](https://gitlab.com/ustervbo/pyci/-/commits/main)
[![Unittest](http://gitlab.com/ustervbo/pyci/-/jobs/artifacts/main/raw/unittest.svg?job=create_badges)](https://gitlab.com/ustervbo/pyci/-/jobs)
[![Coverage](http://gitlab.com/ustervbo/pyci/-/jobs/artifacts/main/raw/coverage.svg?job=create_badges)](https://gitlab.com/ustervbo/pyci/-/jobs)
[![Version](http://gitlab.com/ustervbo/pyci/-/jobs/artifacts/main/raw/version.svg?job=create_badges)](https://gitlab.com/ustervbo/pyci/-/releases)

A simple python `.gitlab-ci`

The products of release jobs are available under [Deployment > Releases](https://gitlab.com/ustervbo/pyci/-/releases)

## A note on cache

The cache is created with:

```{yaml}
variables:
  CONDA_ENV_PREFIX: "$CI_PROJECT_DIR/test-env" 

cache: &global_cache_settings
  key: $CI_COMMIT_REF_SLUG
  paths:
      - $CONDA_ENV_PREFIX/
  policy: pull-push

install_dependencies:
  stage: install_dependencies
  cache:
    # inherit all global cache settings
    <<: *global_cache_settings
  before_script:
    # Make sure we have everything to build conda packages
    - 'apt-get update'
    - 'apt-get install -y build-essential'
    - conda env create --force --prefix=$CONDA_ENV_PREFIX --file=environment.yml
    - source activate $CONDA_ENV_PREFIX
  script:
    - conda info --envs
    - conda install pip hypothesis pytest pytest-cov
    - pip install anybadge
  only:
    changes:
      - environment.yml
```

The cache is then used in each job with:

```{yaml}
cache:
  # inherit all global cache settings
  <<: *global_cache_settings 
  policy: pull
```

`policy: pull` tells the runner not to update the cache.

The cache is updated though `install_dependencies` only when `environment.yml` changes, which is great, because installing the same libraries over and over is a waste of time. The drawback is, that the pipelines will fail, if we clear the cache manually. One way to fix this is addition or removal of a ghost line in the `environment.yml`.

It would be great if we could execute the job if the environment is missing in the `rules` section, then the commit tree is kept clean because there is no addition and removal of ghost lines. However, this is not possible because the job is marked for execution before the cache is downloaded. The behaviour can be achieved by having an `if` statement in the script. In this particular case the script in `install_dependencies` can be refactored to avoid duplication:

```{yaml}
# Job/script to update the conda environment
.setup_conda: &setup_script
  stage: install_dependencies
  before_script: []
  script:
    # Make sure we have everything to build conda packages
    # Create conda environment
    # Activate and install packages
    - >
      if [ $(grep "true" $CI_UPDATE_CONDA | wc -l) == "1" ]; then
        echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        echo "                            Updating conda                           "
        echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        apt-get update
        apt-get install -y build-essential
        echo "Running install"
        conda env create --force --prefix=$CONDA_ENV_PREFIX --file=environment.yml
        source activate $CONDA_ENV_PREFIX
        conda install pip hypothesis pytest pytest-cov
        pip install anybadge
      fi
```

Starting the job with a `.` keeps it from being added to the job graph.

The environment is then updated with:

```{yaml}
env_update:
  stage: update_env
  cache:
    # inherit all global cache settings
    <<: *global_cache_settings
  <<: *setup_script
  before_script:
    - echo "true" > $CI_UPDATE_CONDA
  variables:
    CI_UPDATE_CONDA: "run_conda_update"
  rules:
    # If we have an updated 'environment.yml'
    - changes:
        - environment.yml
```

and created if it does not exist in the cache:

```{yaml}
check_conda:
  stage: install_dependencies
  cache:
    # inherit all global cache settings
    <<: *global_cache_settings
  <<: *setup_script
  before_script:
    # It seems to bet better to check if the environment exists, but for some reason it
    # seems not to work. Checking for the directory is easier
    # Write 'true' is the env does not exist
    - > 
      if [[ ! -d "$CONDA_ENV_PREFIX" ]]; then 
        echo "true" > $CI_UPDATE_CONDA
      else
        echo "false" > $CI_UPDATE_CONDA
      fi
  variables:
    CI_UPDATE_CONDA: "run_conda_update"
```

## A note on releases

It is possible store release artifacts with Gitlab. It is a nice way to store the project in a single file. One can also store builds which is great if one provides install or portable files. See the Gitlab documentation on how to use build artifacts [https://docs.gitlab.com/ee/user/project/releases](https://docs.gitlab.com/ee/user/project/releases).

A job like the one below creates a release when a tag is pushed to the repository:

```{yaml}
release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  # Don't use the cache
  cache: [] 
  # A global `before_script` should also not be run
  before_script: []
  rules:
    - if: $CI_COMMIT_TAG                  # Run this job when a tag is created manually
  script:
    - echo "Running the release job."
  release:
    tag_name: $CI_COMMIT_TAG
    name: 'Release $CI_COMMIT_TAG'
    description: 'Release created using the release-cli.'
```

## Create badges

Badges are a great way of showing various information in an informative and colourful way. Badges can be created in a separate job and uploaded as artifacts. The keyword `when: always` make sure that the artifacts are also uploaded also if the job fails. For some reason, Gitlab only access artifacts if the pipeline succeeds. The effect is, that a unit test badge will show as passed (if the previous pipeline passed) even though the test job in the current pipeline failed. If we want to show the number of failed and passed tests, we have to allow all jobs to fail. See below for an idea on how to still get notifications.

We can add badges in Gitlab to appear on top of the page, of to the read me file.

To add to Gitlab, go to `Settings > General > Badges` and add the following information:

### The pipeline badge

+ Name: Pipeline
+ Link: `https://gitlab.com/%{project_path}/-/commits/%{default_branch}`
+ Badge image URL: `https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg`

### The coverage badge

Make sure the python package `hypothesis` is installed.

+ Name: Coverage
+ Link: `https://gitlab.com/%{project_path}/-/jobs`
+ Badge image URL: `https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg`

In Settings > CI/CD > General pipelines add `^TOTAL.+?(\d+\%)$` in the field 'Test coverage parsing'.

Saved the artifact in the `.gitlab-ci.yml` in the section `reports/coverage_report` like:

```{yaml}
artifacts:
  reports:
    coverage_report:
      coverage_format: cobertura
      path: coverage/cobertura-coverage.xml
```

### The unittest badge

The unittest-badge is a little complicated, but it is not too bad.

Make sure the python package `anybadge` is installed - it must be installed via `pip`.

+ Name: Unittest
+ Link: `https://gitlab.com/%{project_path}/-/jobs`
+ Badge image URL: `http://gitlab.com/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/unittest.svg?job=test`

We run the unittest and export in `junitxml` format. Afterwards we extract the needed information from the file, and string this together to something coherent.

```{yaml}
  allow_failure: true
  script:
    - pytest --junitxml=report.xml
  after_script:
    # Get info from the report xml
    - total=$(sed -n "s/^.*tests=\"\([0-9]*\)\".*$/\1/p" report.xml)
    - errors=$(sed -n "s/^.*errors=\"\([0-9]*\)\".*$/\1/p" report.xml)
    - failures=$(sed -n "s/^.*failures=\"\([0-9]*\)\".*$/\1/p" report.xml)
    - skipped=$(sed -n "s/^.*skipped=\"\([0-9]*\)\".*$/\1/p" report.xml)
    - passed=$(($total-$skipped-$failures-$errors))
    # Put together everything
    - if [[ $failures != "0" ]]; then badge_colour="red"; else badge_colour="green"; fi
    - badge_str="$passed passed, $failures failed"
    - anybadge --label=unittest --value="$badge_str" --file=unittest.svg --color="$badge_colour"
  artifacts:
    when: always
    reports:
      junit: report.xml
    paths:
      - unittest.svg
```

### The latest version badge

The latest version can be obtained from the tag - another good reason to tag stages of development. A bit of searching will provide the command `git describe --tags --abbrev=0` to extract the short name of the last tag, but that apparently does not work very well with Gitlab CI. Instead, we need to fetch the tags (`git fetch --tags`) and extract the last one (`git tag --list | sort -V | tail -n 1`).

```{yaml}
  script:
    - git fetch --tags
    - version_badge_str=$(git tag --list | sort -V | tail -n 1)
    - >
      if [ "$version_badge_str" == "" ]; then
        version_badge_str="none"
      fi
    - version_badge_colour="teal"
    - >
      anybadge --label=version \
               --value="$version_badge_str" \
               --color="$version_badge_colour" \
               --file=version.svg
  artifacts:
    when: always
    paths:
      - version.svg
```


## Adding badges to the README

The standard badges as well as the badges created with `anybadge` can be included in the readme. For the unittest badge above you can:

```{}
[![Unittest](http://gitlab.com/ustervbo/pyci/-/jobs/artifacts/main/raw/unittest.svg?job=unittest_badge)]((https://gitlab.com/ustervbo/pyci/-/jobs))
```

Unless you find it cool to show the unittest results of this repository, the URLs should follow the pattern: `https://example.gitlab.com/<project-path>/-/jobs/artifacts/<branch>/raw/<badge-filename>.svg?job=<job name>)](<link-to-head-when-clicking>)`

## One example

This example should be ready to roll for conda environments. The conda environment can be exported using:

```{}
conda env export  > environment.yml
```

or a more system agnostic environment:

```{}
conda env export --from-history > environment.yml
```

The `.gitlab-ci.yml` can look like this:

```{}
image: continuumio/miniconda3:latest

stages:
  - test
  - coverage

before_script:
  # Make sure we have everything to build conda packages
  - 'apt-get update'
  - 'apt-get install -y build-essential'
  - 'conda env update --name base --file environment.yml'
  - conda install pip hypothesis pytest pytest-cov
  - pip install anybadge

test:
  stage: test
  allow_failure: false
  script:
    - pytest --junitxml=report.xml
  after_script:
    # Get info from the report xml
    - total=$(sed -n "s/^.*tests=\"\([0-9]*\)\".*$/\1/p" report.xml)
    - errors=$(sed -n "s/^.*errors=\"\([0-9]*\)\".*$/\1/p" report.xml)
    - failures=$(sed -n "s/^.*failures=\"\([0-9]*\)\".*$/\1/p" report.xml)
    - skipped=$(sed -n "s/^.*skipped=\"\([0-9]*\)\".*$/\1/p" report.xml)
    - passed=$(($total-$skipped-$failures-$errors))
    # Put together everything
    - if [[ $failures != "0" ]]; then badge_colour="red"; else badge_colour="green"; fi
    - badge_str="$passed passed, $failures failed"
    - anybadge --label=unittest --value="$badge_str" --file=unittest.svg --color="$badge_colour"
    - ls *
  artifacts:
    when: always
    paths:
      - report.xml
      - unittest.svg
    reports:
      junit: report.xml

coverage:
  stage: coverage
  needs: ["test"]
  allow_failure: true
  script:
    - python -V
    - coverage run -m pytest
    # Just to print to the log
    - coverage report
    # Export for later use
    - coverage xml
  artifacts:
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura-coverage.xml


sast:
  stage: test
  before_script: []
include:
- template: Security/SAST.gitlab-ci.yml

```

A full pipeline to test and release a python program (and used here) is given in  `.gitlab-ci.yml` in this repository.

## Sending e-mail in case of pipeline failure

As noted above, we must accept jobs to fail, if we want badges in all circumstances. This means, that we will never get notified if a pipeline fails. One workaround is to have a job, which sends an e-mail in case the unittest failed:

```{yaml}
final_ci_result:
  stage: finalise_pipeline
  image: alpine:latest
  dependencies:
    - "test"
  before_script:
    - apk update
    - apk add ssmtp
  cache: []
  script:
    # Create conf file
    - echo "root=$SMTP_AUTH_USER" > .ssmtp_conf
    - echo "mailhub=smtp.gmail.com:587" >> .ssmtp_conf
    - echo "rewriteDomain=gmail.com" >> .ssmtp_conf
    - echo "TLS_CA_FILE=/etc/ssl/certs/ca-certificates.crt" >> .ssmtp_conf
    - echo "UseTLS=Yes" >> .ssmtp_conf
    - echo "UseSTARTTLS=Yes" >> .ssmtp_conf
    - echo "AuthUser=$SMTP_AUTH_USER" >> .ssmtp_conf
    - echo "AuthPass=$SMTP_AUTH_PASS" >> .ssmtp_conf
    - echo "AuthMethod=LOGIN" >> .ssmtp_conf
    - echo "FromLineOverride=YES" >> .ssmtp_conf
    # Create the email to send
    - >
      echo "Subject: $CI_PROJECT_TITLE | Failed pipeline for $CI_COMMIT_BRANCH | $CI_COMMIT_SHORT_SHA" > email_cont
    - echo "" >> email_cont
    - >
      echo "Pipeline: $CI_PIPELINE_ID" >> email_cont
    - >
      echo "Project: $GITLAB_USER_NAME/$CI_PROJECT_TITLE" >> email_cont
    - >
      echo "Branch: $CI_COMMIT_BRANCH" >> email_cont
    - >
      echo "Commit: $CI_COMMIT_SHORT_SHA/$CI_COMMIT_TITLE" >> email_cont
    - >
      echo "Commit Author: $CI_COMMIT_AUTHOR" >> email_cont
    - echo "" >> email_cont
    # Prepare to send mail
    - PIPELINE_FAILED="false"
    - >
      if [ -f job_status_unittest ]; then
        if [[ $(cat job_status_unittest) == "failed" ]]; then
          PIPELINE_FAILED="true"
        fi
      else
        PIPELINE_FAILED="true"
      fi
    - >
      if [ "$PIPELINE_FAILED" == "true" ]; then
        if [ "$SMTP_AUTH_USER" != "" ]; then
          ssmtp -C .ssmtp_conf -F "GitLab $CI_PROJECT_NAMESPACE" -v $SMTP_EMAIL_TO < email_cont
        else
          echo "--------------------------------------"
          echo "Unittest failed"
          echo
          echo "Could not send email"
          echo "--------------------------------------"
        fi
      fi
    # Cleaning
    - rm .ssmtp_conf email_cont
```

In this example we use `ssmtp` to send an email which require extra work if multiple recipients are required. Here emails are sent via Gmail using an application specific password and all information are stored in CI variables (`Settings > CI/CD > Variables`) with at least the password masked. This way it is more difficult to accidentally give the world access to the email account.

If protected variables are used, they may not be accessible to the development branch, so we check this before actually sending an email.

See here for a description of creating application specific password: [Google Account Help - Sign in with App Passwords](https://support.google.com/accounts/answer/185833?hl=en).

This post on Medium [Send Gitlab CI reports & artifacts via e-mail](https://medium.com/devops-with-valentine/send-gitlab-ci-reports-artifacts-via-e-mail-86bc96e66511) might also be helpful.
